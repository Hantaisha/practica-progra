﻿
namespace CharacterSelect
{
    class Characters
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public float health;
        public float Health
        {
            get
            {
                return health;
            }
            set
            {
                health = value;
            }
        }

        public Characters(string _name)
        {
            name = _name;
        }

        public virtual void Introduction()
        {

        }
    }
}
