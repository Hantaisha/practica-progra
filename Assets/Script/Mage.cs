﻿using UnityEngine;

namespace CharacterSelect
{
    class Mage : Characters
    {
        private float magicPoints;
        public float MagicPoints
        {
            get
            {
                return magicPoints;
            }
            set
            {
                magicPoints = value;
            }
        }

        public Mage(string name) : base(name)
        {

            health = 10f;
            magicPoints = 20f;

        }

        public override void Introduction()
        {
            Debug.Log("Want me to cast something?");
        }
    }
}
