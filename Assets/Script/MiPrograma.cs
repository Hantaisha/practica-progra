﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CharacterSelect;

public class MiPrograma : MonoBehaviour
{
    public InputField inputFieldClass;
    public InputField inputFieldName;
    // Start is called before the first frame update

    void Start()
    {
        Debug.Log("Choose Class: ");
        Debug.Log("Warrior (1)");
        Debug.Log("Mage (2)");
        Debug.Log("Thief (3)");
    }

    public void Show()
    {
        Characters character = null; 
        string chrClass = inputFieldClass.text;
        string hisName = inputFieldName.text;

            switch (chrClass)
            {
                case "1":
                    character = new Warrior(hisName);
                    break;

                case "2":
                    character = new Mage(hisName);
                    break;

                case "3":
                    character = new Thief(hisName);
                    break;
            }


            Debug.Log("Character Name is: " + character.Name);
        
        character.Introduction();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
